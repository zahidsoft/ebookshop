<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- admin panel link -->
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">


    <!-- add custom css  -->
  <link rel="stylesheet" href="{{asset('public/frontend/css/style.css')}}">
  <!-- bootstrap  -->
  <link rel="stylesheet" href="{{asset('public/frontend/css/bootstrap.min.css')}}">
   
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

   <!-- slider div slick -->
   <link rel="stylesheet" type="text/css" href="{{asset('public/frontend/slick/slick.css')}}"/>
  <link rel="stylesheet" type="text/css" href="{{asset('public/frontend/slick/slick-theme.css')}}"/>

   <!-- toasts notification -->
   <link rel="stylesheet" href="http://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">
    @stack('css')
</head>
<body>



@yield('contain')



<script type="text/javascript" src="{{asset('public/frontend/js/jquery.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/frontend/js/mystyle.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/frontend/js/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('public/frontend/slick/slick.min.js')}}"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.slick-zaslider').slick({
      slidesToShow: 5,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
    });
  });
</script>

<script src="http://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>
        {!! Toastr::message() !!}

@stack('js')
</body>
</html>
