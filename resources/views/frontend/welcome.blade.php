@extends('layouts.frontend.app')
@section('title','HOME')

@push('css')

@endpush
@section('contain')
<div class="container-fluid">
  <div class="">
    <div class="slick-zaslider">
     @foreach($sliderPosts as $sliderPost)
      <div class="card-content m-4" id="slick-slidewp">
          <div class="card-img">
                <img src="{{asset('storage/app/public/post/'.$sliderPost->post->image)}}" alt="">
                        
          </div>
          <div class="card-desc">
                <h3 class="text-center">{{ Illuminate\Support\Str::limit($sliderPost->post->title, 30, $end='....') }}</h3>
                <p class="text-center">{{$sliderPost->post->authorName}}</p>
                  <a href="{{ route('book.details',$sliderPost->post_id)}}" class="btn-card">Read</a>  
          </div>
      </div>
      @endforeach
    </div>
  </div>
</div>



<div class="container-fluid">
  <div class="row">
    <!-- details card section starts from here -->
<section class="details-card">
    <div class="container-fluid">
        <div class="row">
          @foreach($posts as $post)
            <div class="col-md-3 mb-4">
                <div class="card-content">
                    <div class="card-img">
                        <img src="{{asset('storage/app/public/post/'.$post->image)}}" alt="book Image">
                        
                    </div>
                    <div class="card-desc">
                        <h3 class="text-center">{{ Illuminate\Support\Str::limit($post->title, 30, $end='....') }}</h3>
                        <p class="text-center">{{$post->authorName}}</p>
                            <a href="{{ route('book.details',$post->id)}}" class="btn-card">Read More</a>
                            <!-- <a href="{{ route('buy.book',$post->id)}}" class="btn-card">Buy Now</a>    -->
                    </div>
                </div>
            </div>
          @endforeach
        </div>
        <div class="pl-5">
        {{ $posts->links()}}
        </div>
    </div>
</section>
<!-- details card section starts from here -->
  </div>
</div>
@endsection

@push('js')
@endpush