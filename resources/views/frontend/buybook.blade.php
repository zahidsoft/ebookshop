@extends('layouts.frontend.app')
@section('title','Buy Book')

@push('css')
<style>
    .divider-text {
    position: relative;
    text-align: center;
    margin-top: 15px;
    margin-bottom: 15px;
}
.divider-text span {
    padding: 7px;
    font-size: 12px;
    position: relative;   
    z-index: 2;
}
.divider-text:after {
    content: "";
    position: absolute;
    width: 100%;
    border-bottom: 1px solid #ddd;
    top: 55%;
    left: 0;
    z-index: 1;
}

.btn-facebook {
    background-color: #405D9D;
    color: #fff;
}
.btn-twitter {
    background-color: #42AEEC;
    color: #fff;
}
  </style>
@endpush

@section('contain')
<div class="container">
<br>  <p class="text-center" style="font-size: 35px;">
অর্ডার সাবমিট করার জন্য নিচের দুইটি স্টেপ পূরণ করুন। <br>
১ প্রথমে আমাদের বিকাশ পার্শোনাল নাম্বারে ৫০ টাকা সেন্ড করুন। <br>
<strong>( বিকাশ পার্শেোনাল নাম্বার 01312808055) </strong><br>
২ তারপর নিচের ফর্মটি পূরণ করুন।
</p>
<hr>





<div class="card bg-light">
<article class="card-body mx-auto" style="max-width: 400px;">
  <h4 class="card-title mt-3 text-center">Buy The Book</h4>
  <p class="text-center">Buy Your Books</p>
  <p>
    <a href="{{route('welcome')}}" class="btn btn-block btn-twitter">Back To Buy Another</a>
  </p>

  <form action="{{route('addOrder')}}" method="POST">
  @csrf
  <div class="form-group input-group">
    <div class="input-group-prepend">
        <span class="input-group-text"> <i class="fa fa-user"></i> </span>
     </div>
        <input name="name" class="form-control" placeholder="নাম" type="text">
    </div> <!-- form-group// -->
    <div class="form-group input-group">
    <div class="input-group-prepend">
        <span class="input-group-text"> <i class="fa fa-user"></i> </span>
     </div>
        <input name="thana" class="form-control" placeholder="থানা" type="text">
    </div> <!-- form-group// -->
    <div class="form-group input-group">
    <div class="input-group-prepend">
        <span class="input-group-text"> <i class="fa fa-user"></i> </span>
     </div>
        <input name="jela" class="form-control" placeholder="জেলা" type="text">
    </div> <!-- form-group// -->
    <div class="form-group input-group">
    <div class="input-group-prepend">
        <span class="input-group-text"> <i class="fa fa-user"></i> </span>
     </div>
        <input name="postOffic" class="form-control" placeholder="পোষ্ট অফিস" type="text">
    </div> <!-- form-group// -->
    <div class="form-group input-group">
    <div class="input-group-prepend">
        <span class="input-group-text"> <i class="fa fa-building"></i> </span>
     </div>
        <input name="postCod" class="form-control" placeholder="পোষ্ট কোড" type="number">
    </div> <!-- form-group// -->
    <div class="form-group input-group">
      <div class="input-group-prepend">
        <span class="input-group-text"> <i class="fa fa-phone"></i> </span>
    </div>
      <input name="mobileNumber1" class="form-control" placeholder="মোবাইল নাম্বার" type="number">
    </div>
    <div class="form-group input-group">
      <div class="input-group-prepend">
        <span class="input-group-text"> <i class="fa fa-phone"></i> </span>
    </div>
      <input name="mobileNumber2" class="form-control" placeholder="মোবাইল নাম্বার 2" type="number">
    </div> <!-- form-group// -->
    <div class="form-group input-group">
      <div class="input-group-prepend">
        <span class="input-group-text"> <i class="fa fa-phone"></i> </span>
    </div>
      <input name="bkashNumber" class="form-control" placeholder="বিকাশ সেন্ডার নাম্বার" type="number">
      <input name="post_id" class="form-control" value="{{$id}}" type="hidden">
      <input name="isApprove" class="form-control" value="0" type="hidden">
    </div> <!-- form-group// -->                                      
    <div class="form-group">
        <button type="submit" class="btn btn-primary btn-block"> Submit  </button>
    </div> <!-- form-group// -->                     
</form>
</article>
</div> <!-- card.// -->

</div> 
<!--container end.//-->

<br><br>
<article class="bg-secondary mb-3">  
<div class="card-body text-center">
    <h3 class="text-white mt-3">অফিস</h3>
<p class="h5 text-white">মোবাইল নাম্বার  <br> for নাম, মোবাইল নাম্বার, মোবাইল নাম্বার, পোষ্ট অফিস, পোষ্ট কোড, থানা, জেলা,</p>   <br>
</div>
<br><br>
</article>

@endsection

@push('js')
@endpush