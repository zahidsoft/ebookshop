@extends('layouts.frontend.app')
@section('title','HOME')

@push('css')
<style>
table, td, th {  
  border: 1px solid #ddd;
  text-align: left;
}

table {
  border-collapse: collapse;
  width: 100%;
}
      th, td {
  padding: 15px;
  text-align: center;
}
  </style>
@endpush
@section('contain')
<div class="container m-3 ">
    <div class="row">
        <div class="col-md-12 ">
            <a  href="{{route('welcome')}}"><button class="btn btn-success">BACK TO HOME PAGE</button> </a>
        </div>
    </div>
</div>
<div class="container" style="background-color: white;">
    <div class="row">
        <div class="col-md-6 ">
            <img class="img-fluid mt-2 border" style="object-fit: cover;" src="{{asset('storage/app/public/post/'.$post->image)}}" alt="book images">
            
        </div>
        <div class="col-md-6 p-5">
            <p>{{$post->title}}</p><br>
            <strong class="pb-3">TK. {{$post->price}}</strong>
            <a class="align-middle pt-5" href="{{ route('buy.book',$post->id)}}"><button class="btn btn-outline-success">BUY NOW</button> </a>

        </div>
    </div>
</div>
<div class="container mt-4" style="background-color: white;">
    <div class="row">
        <div>
            <nav>
  <div class="nav nav-tabs" id="nav-tab" role="tablist">
    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">বইটি কেন পড়বেন</a>
    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">বই</a>
    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">লেখকের কথা</a>
  </div>
</nav>
<div class="tab-content" id="nav-tabContent">
  <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
    <p class="p-4">{!!$post->body!!}</p>
</div>
  <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
  <table>
      <thead>
          <tr>
              <th>Title</th>
              <th>Description</th>
          </tr>
      </thead>
      <tbody>
          <tr>
              <td>Author</td>
              <td> {{$post->authorName}}</td>
          </tr>
          <tr>
              <td>Title</td>
              <td> {{$post->title}}</td>
          </tr>
      </tbody>

  </table>
</div>
  <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
  <P class="p-4">{!!$post->aboutAuthor!!}</P>
</div>
</div>
        </div>
    </div>
</div>








<div class="container-fluid">
  <div class="row">
    <!-- details card section starts from here -->
<section class="details-card">
    <div class="container-fluid">
        <div class="row">
         @foreach($randomposts as $randompost)
            <div class="col-md-2 mb-4">
                <div class="card-content">
                    <div class="card-img">
                        <img src="{{asset('storage/app/public/post/'.$randompost->image)}}" alt="">
                        
                    </div>
                    <div class="card-desc">
                        <h3 class="text-center">{{ Illuminate\Support\Str::limit($randompost->title, 30, $end='....') }}</h3>
                        <p class="text-center">{!!$randompost->authorName!!}</p>
                            <!-- <a href="{{ route('book.details',$randompost->id)}}" class="btn-card">Read More</a> -->
                            <a href="{{ route('buy.book',$randompost->id)}}" class="btn-card">Buy Now</a>    
                    </div>
                </div>
            </div>
          @endforeach

        </div>
    </div>
</section>
<!-- details card section starts from here -->
  </div>
</div>
@endsection

@push('js')
@endpush