@extends('layouts.admin.app')

@push('css')
@endpush

@section('contain')
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="container-fluid">
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                    ALL Order
                                    <span class="badge bg-blue">{{ $orders->count() }}</span>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Thana</th>
                                            <th>Jela</th>
                                            <th>Post Office</th>
                                            <th>Post Code</th>
                                            <th>Mobile Nu-1</th>
                                            <th>Mobile Nu-</th>
                                            <th>Bkash M-N</th>
                                            <th>Image</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Thana</th>
                                            <th>Jela</th>
                                            <th>Post Office</th>
                                            <th>Post Code</th>
                                            <th>Mobile Nu-1</th>
                                            <th>Mobile Nu-2</th>
                                            <th>Bkash s-N</th>
                                            <th>Image</th>
                                            <th>Action</th>
                                        </tr>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        @foreach($orders as $key=>$order)
                                            <tr>
                                                <td>{{ $key + 1}}</td>
                                                <td>{{$order->name}}</td>
                                                <td>{{$order->thana}}</td>
                                                <td>{{$order->jela}}</td>
                                                <td>{{$order->postOffic}}</td>
                                                <td>{{$order->postCod}}</td>
                                                <td>{{$order->mobileNumber1}}</td>
                                                <td>{{$order->mobileNumber2}}</td>
                                                <td>{{$order->bkashNumber}}</td>

                                                
                                                <td>
                                                    <img src="{{asset('storage/app/public/post/'.$order->post->image)}}" height="130px" width="100px" alt="book">
                                                </td>
                                                <td class="text-center">
                                                    <a href="{{route('admin.order.view',$order->id)}}" class="btn btn-info waves-effect">
                                                        <i class="material-icons">visibility</i>
                                                    </a>
                                                    
                                                    <form id="delete-form-" action="{{route('admin.order.destroy',$order->id)}}" method="POST" >
                                                        @csrf
                                                        @method('DELETE')
                                                        <button class="btn btn-danger waves-effect" type="submit">
                                                             <i class="material-icons">delete</i>
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                    {{ $orders->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>

        </div>
     </div>
</section>

@endsection


@push('js')
@endpush