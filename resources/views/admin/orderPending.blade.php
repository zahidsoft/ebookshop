@extends('layouts.admin.app')

@section('title','panding order')

@push('css')
@endpush

@section('contain')
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="container-fluid">
            <div class="block-header">
                
            </div>
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                    ALL pending order
                                    <span class="badge bg-blue">{{ $pendings->count() }}</span>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Thana</th>
                                            <th>Jela</th>
                                            <th>Post Office</th>
                                            <th>Post Code</th>
                                            <th>Mobile Nu-1</th>
                                            <th>Mobile Nu-</th>
                                            <th>Bkash M-N</th>
                                            <th>Image</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Thana</th>
                                            <th>Jela</th>
                                            <th>Post Office</th>
                                            <th>Post Code</th>
                                            <th>Mobile Nu-1</th>
                                            <th>Mobile Nu-</th>
                                            <th>Bkash M-N</th>
                                            <th>Image</th>
                                            <th>Action</th>
                                        </tr>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        @foreach($pendings as $key=>$pending)
                                            <tr>
                                                <td>{{ $key + 1}}</td>
                                                <td>{{$pending->name}}</td>
                                                <td>{{$pending->thana}}</td>
                                                <td>{{$pending->jela}}</td>
                                                <td>{{$pending->postOffic}}</td>
                                                <td>{{$pending->postCod}}</td>
                                                <td>{{$pending->mobileNumber1}}</td>
                                                <td>{{$pending->mobileNumber2}}</td>
                                                <td>{{$pending->bkashNumber}}</td>

                                                
                                                <td>
                                                    <img src="{{asset('storage/app/public/post/'.$pending->post->image)}}" height="130px" width="100px" alt="book">
                                                </td>
                                                <td class="text-center">
                                                    <a href="{{route('admin.order.view',$pending->id)}}" class="btn btn-info waves-effect">
                                                        <i class="material-icons">visibility</i>
                                                    </a>
                                                    <a href="{{route('admin.order.approve',$pending->id)}}" class="btn btn-info waves-effect">
                                                        approve
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>

        </div>
     </div>
</section>

@endsection


@push('js')
@endpush