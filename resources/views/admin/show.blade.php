@extends('layouts.admin.app')
@section('title','post show')

@section('contain')
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
        <div class="container-fluid">
            <!-- Vertical Layout | With Floating Label -->
         
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Show Post
                            </h2>
                        </div>
                        <div class="body">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                       <strong>Author Name:</strong> <p>{{$post->authorName}}</p>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <strong>Title:</strong>
                                        <p>{{$post->title}}</p>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <strong>Price:</strong>
                                        <p>{{$post->price}}</p>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <img src="{{asset('storage/app/public/post/'.$post->image)}}" class="img-fluid" alt="book Image">
                                </div>
                                <div class="form-group">
                                    <strong>Publication Status:</strong>
                                    <p>{{$post->status == true ? 'published' : 'Unpublish'}}</p>
                                </div>

                        </div>
                    </div>
                </div>
            </div>


             <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Description
                            </h2>
                        </div>
                        <div class="body">
                            {!!$post->body!!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                About Author
                            </h2>
                        </div>
                        <div class="body">
                            {!!$post->aboutAuthor!!}
                        </div>
                        <br>
                                
                    </div>
                </div>
            </div>
            <a href="{{route('admin.index')}}" class="btn btn-danger m-t-15 waves-effect">Back</a>
           
        </div>

        </div>
     </div>
</section>
@endsection

@push('js')
@endpush