@extends('layouts.admin.app')

@push('css')
@endpush

@section('contain')
<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Profile</h2>
                </div>
                    <!-- CPU Usage -->
                    <div class="row clearfix">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                        <div class="header">
                            <h2>
                                VERTICAL LAYOUT
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>
                                        <li><a href="javascript:void(0);" class=" waves-effect waves-block">Another action</a></li>
                                        <li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <form method="POST" action="{{route('admin.pasword.update')}}">
                                @csrf 
                                @method('PUT')
                            <label for="email_address">Old Password</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input name="old_password" type="password" id="email_address" class="form-control" placeholder="Enter your old password">
                                    </div>
                                </div>
                                <label for="email_address">New Password</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input name="password" type="password" id="email_address" class="form-control" placeholder="Enter your new password">
                                    </div>
                                </div>
                                <label for="password">Retype your New Password</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input name="password_confirmation" type="password" id="password" class="form-control" placeholder="Retype your new password">
                                    </div>
                                </div>
                                <br>
                                <button type="submit" class="btn btn-primary m-t-15 waves-effect">Submit</button>
                            </form>
                        </div>
                    </div>
                    </div>
                 </div>
                </div>
            </div>
        </div>
</section>


<!-- user name and email change -->


<section class="content">
        <div class="container-fluid">
            <div class="block-header">
                </div>
                    <!-- CPU Usage -->
                    <div class="row clearfix">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                        <div class="body">
                            <form method="POST" action="{{route('admin.useremail.update')}}">
                                @csrf 
                                @method('PUT')
                                <label for="email_address">Your Email</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input name="email" type="text" id="email_address" class="form-control" placeholder="{{ Auth::user()->email}}" required>
                                    </div>
                                </div>
                                <label for="email_address">Enter Your User Name</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input name="userName" type="text" id="email_address" class="form-control" placeholder="{{ Auth::user()->name}}" required>
                                    </div>
                                </div>
                                <br>
                                <button type="submit" class="btn btn-primary m-t-15 waves-effect">Submit</button>
                            </form>
                        </div>
                    </div>
                    </div>
                 </div>
                </div>
            </div>
        </div>
</section>
@endsection


@push('js')
@endpush