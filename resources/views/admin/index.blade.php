@extends('layouts.admin.app')

@push('css')
@endpush

@section('contain')
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="container-fluid">
            <div class="block-header">
                <a class="btn btn-primary waves-effect" href="{{route('admin.post.create')}}">
                	<i class="material-icons">add</i>
                	<span>Add New Post</span>
            	</a>
            </div>
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                    ALL Post
                                    <span class="badge bg-blue">{{ $posts->count() }}</span>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Author Name</th>
                                            <th>Title</th>
                                            <th>Price</th>
                                            <th>Status</th>
                                            <th>Image</th>
                                            <th>Updated At</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <tr>
                                            <th>ID</th>
                                            <th>Author Name</th>
                                            <th>Title</th>
                                            <th>Price</th>
                                            <th>Status</th>
                                            <th>Image</th>
                                            <th>Updated At</th>
                                            <th>Action</th>
                                        </tr>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        @foreach($posts as $key=>$post)
                                            <tr>
                                                <td>{{ $key + 1}}</td>
                                                <td>{{$post->authorName}}</td>
                                                <td>{{ Illuminate\Support\Str::limit($post->title, 30, $end='....')}}</td>
                                                <td>{{$post->price}}</td>
                                                <td>
                                                    @if($post->status == true)
                                                     <span class="badge bg-blue">Approved</span>
                                                     @else
                                                     <span class="badge bg-pink">Pending</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    <img src="{{asset('storage/app/public/post/'.$post->image)}}" height="130px" width="100px" alt="book">
                                                </td>
                                                <td>{{ $post->updated_at}}</td>
                                                <td class="text-center">
                                                    <a href="{{route('admin.post.show',$post->id)}}" class="btn btn-info waves-effect">
                                                        <i class="material-icons">visibility</i>
                                                    </a>
                                                    <a href="{{route('admin.post.edit',$post->id)}}" class="btn btn-info waves-effect">
                                                        <i class="material-icons">edit</i>
                                                    </a>
                                                    
                                                    <form id="delete-form-" action="{{route('admin.destroy',$post->id)}}" method="POST" >
                                                        @csrf
                                                        @method('DELETE')
                                                        <button class="btn btn-danger waves-effect" type="submit">
                                                             <i class="material-icons">delete</i>
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{$posts->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->
        </div>

        </div>
     </div>
</section>

@endsection


@push('js')
@endpush