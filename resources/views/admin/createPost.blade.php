@extends('layouts.admin.app')
@section('title','post Create')

@section('contain')
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
        <div class="container-fluid">
            <!-- Vertical Layout | With Floating Label -->
         <form action="{{route('admin.post.stor')}}" method="POST" enctype="multipart/form-data">
              @csrf
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Add New Post
                            </h2>
                        </div>
                        <div class="body">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" id="AuthorName" name="authorName" class="form-control" required>
                                        <label class="form-label">Author Name</label>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" id="title" name="title" class="form-control" required>
                                        <label class="form-label">Post Title</label>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="number" id="price" name="price" class="form-control" required>
                                        <label class="form-label">Price</label>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <label for="image">Feature Image</label>
                                    input
                                    <input type="file" name="image" accept="image/*">
                                </div>
                                <div class="form-group">
                                    <input type="checkbox" id="publish" class="filled-in" name="status" value="1">
                                    <label for="publish">Publish</label>
                                </div>

                        </div>
                    </div>
                </div>
            </div>


             <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Description
                            </h2>
                        </div>
                        <div class="body">
                            <textarea id="tinymce" name="body"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                About Author
                            </h2>
                        </div>
                        <div class="body">
                            <textarea id="tinymce" name="aboutAuthor"></textarea>
                        </div>
                        <br>
                                
                    </div>
                </div>
            </div>
            <a href="{{route('admin.index')}}" class="btn btn-danger m-t-15 waves-effect">Back</a>
            <button type="submit" class="btn btn-primary m-t-15 waves-effect">Submit</button>
         </form> 
        </div>

        </div>
     </div>
</section>
@endsection

@push('js')
<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
  <script>tinymce.init({selector:'textarea'});</script>
@endpush