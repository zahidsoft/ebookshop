@extends('layouts.admin.app')
@section('title','order show')

@section('contain')
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
        <div class="container-fluid">
            <!-- Vertical Layout | With Floating Label -->
         
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Show Order
                            </h2>
                        </div>
                        <div class="body">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                       <strong>নাম:</strong> <p>{{$orderV->name}}</p>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <strong>থানা:</strong>
                                        <p>{{$orderV->thana}}</p>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <strong>জেলা:</strong>
                                        <p>{{$orderV->jela}}</p>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <strong>পোষ্ট অফিস:</strong>
                                        <p>{{$orderV->postOffic}}</p>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <strong>পোষ্ট কোড:</strong>
                                        <p>{{$orderV->postCod}}</p>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <strong>মোবাইল নাম্বার 1:</strong>
                                        <p>{{$orderV->mobileNumber1}}</p>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <strong>মোবাইল নাম্বার 2:</strong>
                                        <p>{{$orderV->mobileNumber2}}</p>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <strong>বিকাশ সেন্ডার নাম্বার:</strong>
                                        <p>{{$orderV->bkashNumber}}</p>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <strong>jela:</strong>
                                        <p>{{$orderV->post->title}}</p>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <img src="{{asset('storage/app/public/post/'.$orderV->post->image)}}" class="img-fluid" alt="book Image">
                                </div>
                                <div class="form-group">
                                    <strong>Publication Status:</strong>
                                    <p>{{$orderV->isApprove == true ? 'Approve' : 'Pendding'}}</p>
                                </div>

                        </div>
                    </div>
                </div>
            </div>


            <a href="{{route('admin.all.order')}}" class="btn btn-danger m-t-15 waves-effect">Back</a>
           
        </div>

        </div>
     </div>
</section>
@endsection

@push('js')
@endpush