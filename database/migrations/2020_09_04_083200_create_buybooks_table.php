<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBuybooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buybooks', function (Blueprint $table) {
            $table->id('id');
            $table->foreignId('post_id');
            $table->boolean('isApprove')->default(false);
            $table->string('name');
            $table->string('thana');
            $table->string('jela');
            $table->string('postOffic');
            $table->string('postCod');
            $table->string('mobileNumber1');
            $table->string('mobileNumber2')->nullable();
            $table->string('bkashNumber');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buybooks');
    }
}
