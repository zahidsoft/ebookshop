<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','Frontend\FrontendPostController@index')->name('welcome');
Route::get('buy/book/{id}','BuybookController@index')->name('buy.book');
Route::post('book/order','BuybookController@stor')->name('addOrder');


Route::get('book/details/{id}','Frontend\FrontendPostController@bookdetails')->name('book.details');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['as'=>'admin.','prefix'=>'admin','namespace'=>'Admin','middleware'=>['auth']],function(){
    Route::get('settings','UserController@index')->name('settings');
    Route::put('update-password','UserController@updatepassword')->name('pasword.update');
    Route::put('userNameEmail','UserController@userNameEmail')->name('useremail.update');
    route::get('showAllBook','PostController@index')->name('index');
    Route::get('addPost','PostController@postCreate')->name('post.create');
    Route::post('postStor','PostController@postStor')->name('post.stor');
    Route::get('ShowPOst/{id}','PostController@showPost')->name('post.show');
    Route::get('postEdit/{id}','PostController@editPost')->name('post.edit');
    Route::put('post/update/{id}','PostController@updatePost')->name('post.update');
    Route::delete('post/delete/{id}','PostController@destroy')->name('destroy');


    Route::get('all/order','OrderController@index')->name('all.order');
    Route::get('order/view/{id}','OrderController@orderview')->name('order.view');
    Route::delete('order/delete/{id}','OrderController@destroyOrder')->name('order.destroy');
    Route::get('pending/order','OrderController@pendingOrder')->name('pending.order');
    Route::get('order/approve/{id}','OrderController@approveOrder')->name('order.approve');
});
