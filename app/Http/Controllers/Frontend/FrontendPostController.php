<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Post;
use App\Buybook;
use Illuminate\Support\Str;

class FrontendPostController extends Controller
{
    public function index(){
        $sliderPosts = Buybook::latest()->take(6)->get();
        $posts = Post::latest()->published()->paginate(12);
        return view('frontend.welcome',compact('posts','sliderPosts'));
    }

    public function bookdetails($id){
        $post = Post::find($id);
        $randomposts = Post::published()->take(6)->inRandomOrder(6)->get();
        return view('frontend.postDetails',compact('post','randomposts'));
    }
}
