<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Buybook;
use Brian2694\Toastr\Facades\Toastr;

class BuybookController extends Controller
{
    public function index($id){
        return view('frontend.buybook',compact('id'));
    }

    public function stor(Request $request){
        $this->validate($request,[
            'name' => 'required',
            'thana' => 'required',
            'jela' => 'required',
            'postOffic' => 'required',
            'postCod' => 'required',
            'mobileNumber1' => 'required',
            'bkashNumber' => 'required'
        ]);
        $addOrder = new Buybook();
        $addOrder->post_id = $request->post_id;
        $addOrder->isApprove = $request->isApprove;
        $addOrder->name = $request->name;
        $addOrder->thana = $request->thana;
        $addOrder->jela = $request->jela;
        $addOrder->postOffic = $request->postOffic;
        $addOrder->postCod = $request->postCod;
        $addOrder->mobileNumber1 = $request->mobileNumber1;
        $addOrder->mobileNumber2 = $request->mobileNumber2;
        $addOrder->bkashNumber = $request->bkashNumber;
        $addOrder->save();

        Toastr::success('Add Your Order Successfully','success');
        return redirect()->route('welcome');
    }
}
