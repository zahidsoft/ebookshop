<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\User;
use Brian2694\Toastr\Facades\Toastr;

class UserController extends Controller
{
    public function index(){
        return view('admin.settings');
    }

    public function updatepassword(Request $request){
        $this->validate($request,[
            'old_password'=> 'required',
            'password'=> 'required|confirmed'
        ]);

        $ancriptPassword = Auth::user()->password;
        if (Hash::check($request->old_password,$ancriptPassword)) {
            if (!Hash::check($request->password,$ancriptPassword)) {
                $user = User::find(Auth::id());
                $user->password = Hash::make($request->password);
                $user->save();
                Auth::logout();
                return redirect()->back();
            } else {
                Toastr::error('New Password Cannot Be The Same As Old Password','Error');
			return redirect()->back();
            }
        } else {
            Toastr::error('Current and Old password Not Match','Error');
            return redirect()->back();
        }
        
    }

    public function userNameEmail(Request $request){
        $this->validate($request,[
            'email' => 'required|email',
            'userName' => 'required'
        ]);

        $user = User::findOrfail(Auth::id());

        $user->name = $request->userName;
        $user->email = $request->email;
        $user->save();

        Toastr::success('User name and email change Successfully','success');
        return redirect()->back();

    }




}
