<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Post;
use Brian2694\Toastr\Facades\Toastr;

class PostController extends Controller
{
    public function index(){
        $posts = Post::latest()->paginate(10);
        return view('admin.index',compact('posts'));
    }

    public function showPost($id){
        $post = Post::find($id);
        return view('admin.show',compact('post'));
    }

    public function postCreate(){
        return view('admin.createPost');
    }
    
    public function postStor(Request $request){
        $this->validate($request,[
            'authorName' => 'required',
            'title' => 'required',
            'price' => 'required',
            'body' => 'required',
            'aboutAuthor' => 'required',
        ]);

        $image = $request->file('image');
        $slug = str::slug($request->title);
        if (isset($image)) {
            $currentDate = Carbon::now()->toDateString();
            $imageName = $slug.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();

            if (!Storage::disk('public')->exists('post')) {
                Storage::disk('public')->makeDirectory('post');
            } 

            Image::make($image)->resize(360,400)->save('public/storage/post/'.$imageName);


        } else {
            $imageName = 'default.png';
        }

        $post = new Post();
        $post->authorName = $request->authorName;
        $post->title = $request->title;
        $post->price = $request->price;
        $post->image = $imageName;
        if (isset($request->status)) {
            $post->status = true;
        } else {
            $post->status = false;
        }
        $post->body = $request->body;
        $post->aboutAuthor = $request->aboutAuthor;
        $post->save();

        Toastr::success('One Book Add Success Fully','success');
        return redirect()->route('admin.index');
        
    }


    public function editPost($id){
        $post = Post::find($id);
        return view('admin.edit',compact('post'));
    }

    public function updatePost(Request $request,$id){
        $this->validate($request,[
            'authorName' => 'required',
            'title' => 'required',
            'price' => 'required',
            'body' => 'required',
            'aboutAuthor' => 'required',
        ]);
        $post = Post::find($id);
        $image = $request->file('image');
        $slug = str::slug($request->title);
        if (isset($image)) {
            $currentDate = Carbon::now()->toDateString();
            $imageName = $slug.'-'.$currentDate.'-'.uniqid().'.'.$image->getClientOriginalExtension();

            if (!Storage::disk('public')->exists('post')) {
                Storage::disk('public')->makeDirectory('post');
            }
            if (Storage::disk('public')->exists('post/'.$post->image)) {
                Storage::disk('public')->delete('post/'.$post->image);
            }

            Image::make($image)->resize(1600,1066)->save('public/storage/post/'.$imageName);


        } else {
            $imageName = 'default.png';
        }

        // $post = new Post();
        $post->authorName = $request->authorName;
        $post->title = $request->title;
        $post->price = $request->price;
        $post->image = $imageName;
        if (isset($request->status)) {
            $post->status = true;
        } else {
            $post->status = false;
        }
        $post->body = $request->body;
        $post->aboutAuthor = $request->aboutAuthor;
        $post->save();

        Toastr::success('Post Update Successfully','success');
        return redirect()->route('admin.index');
    }

    public function destroy($id){
        $post = Post::find($id);
        if (Storage::disk('public')->exists('post/'.$post->image)) {
            Storage::disk('public')->delete('post/'.$post->image);
        }
        $post->delete();

        Toastr::success('Post Delete Successfully','success');
        return redirect()->back();
    }
}
