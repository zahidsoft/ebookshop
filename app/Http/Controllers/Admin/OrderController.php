<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Buybook;
use Brian2694\Toastr\Facades\Toastr;

class OrderController extends Controller
{
    public function index(){
        $orders = Buybook::latest()->paginate(10);
        return view('admin.allorder',compact('orders'));
    }

    public function orderview($id){
        $orderV = Buybook::find($id);
        return view('admin.ordershow',compact('orderV'));
    }
    public function destroyOrder($id){
        $deleteOrder = Buybook::find($id);
        $deleteOrder->delete();
        Toastr::success('Order delete successfully','success');
        return redirect()->back();
    }

    public function pendingOrder(){
        $pendings = Buybook::latest()->approved()->get();
        return view('admin.orderPending',compact('pendings'));
    }

    public function approveOrder($id){
        $approve = Buybook::find($id);
        $approve->isApprove = true;
        $approve->save();
        Toastr::success('Your order approve successfully','success');
        return redirect()->route('admin.index');


    }
}
