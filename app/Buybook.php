<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buybook extends Model
{
    public function post(){
        return $this->belongsTo('App\Post');
    }

    public function scopeApproved($query){
        return $query->where('isApprove',0);
    }
}
